#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 23 11:31:37 2018

@author: martin
"""

from netCDF4 import Dataset
import numpy as np

def write_data(name,data,lats,lons):
    dataset = Dataset(name, 'w',  format='NETCDF4_CLASSIC') 
    #create dimensions
    dataset.createDimension('lev', 6) 
    dataset.createDimension('lat', 720)
    dataset.createDimension('lon', 1440) 
    dataset.createDimension('time', 1 )
    #create 1-d variables
    dataset.createVariable('time', np.int32, ('time',)) 
    dataset.createVariable('lev', np.int32 , ('lev',)) 
    dataset.createVariable('lat', np.float32,   ('lat',))
    dataset.createVariable('lon', np.float32,  ('lon',)) 
    # Create the actual 2-d variable
    dataset.createVariable('T-diagnotic', np.float32, ('lat','lon')) 
    
    dataset['T-diagnotic'][:] = data
    dataset["lat"][:]  = lats  # (-90,90)   theta
    dataset["lon"][:]  = lons 
    
    dataset.close()
        
    return()
