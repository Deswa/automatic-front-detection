#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 19:11:39 2018

@author: martin
"""

from netCDF4 import Dataset
import numpy as np
#Pomocné moduly
import computation as cp
import netCDFfunctions

#Načtení dat
file_T = Dataset("ICON_iko_pressure_level_elements_world_T_2018051400_000.grib2_0.250x0.250.nc","r")
file_V = Dataset("ICON_iko_pressure_level_elements_world_V_2018051400_000.grib2_0.250x0.250.nc","r")
file_U = Dataset("ICON_iko_pressure_level_elements_world_U_2018051400_000.grib2_0.250x0.250.nc","r")

#Načtení proměnných
teplota = file_T['t'][0,1,] #bereme v 950 hp
lats = file_T["lat"][:]  # (-90,90)   theta
lons = file_T["lon"][:]  # (-180,180) 
U = file_U['u'][0,1,]
V = file_V['v'][0,1,]

lats = np.pi/2.0 - lats/180.0*np.pi # polar angle = theta (0,pi)
lons = np.pi + lons/180.0*np.pi # azimuth = lambda (-pi,pi)


#Vyhlazení proměnných - zatim jsem pouzuval pouze 2 iterace z časových duvodu ladeni kodu
teplota = cp.smooth(teplota,2)

#převedení na potencionální teplotu
potencial_temperature=teplota*(10000/file_T.variables["lev"][1])**(0.286)

#výpočet frontálního parametru
a = -cp.spherical_gradient(cp.spherical_gradient(potencial_temperature,lats,lons,norm=True),lats,lons)
b = cp.spherical_gradient(potencial_temperature,lats,lons)/cp.spherical_gradient(potencial_temperature,lats,lons,norm=True)
parametr = np.abs(a[0]*b[0]+a[1]*b[1])

#zápis do souboru
netCDFfunctions.write_data("T-diagnostic.nc",parametr,file_T["lat"][:],file_T["lon"][:])
